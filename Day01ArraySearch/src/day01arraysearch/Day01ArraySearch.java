package day01arraysearch;

import java.util.Scanner;

public class Day01ArraySearch {

    static int getIfExistsGentle(int[][] data, int row, int col) {
        if (row >= data.length || row < 0) {
            return 0; // no such row
        }
        if (col >= data[row].length || col < 0) {
            return 0; // no such row
        }
        return data[row][col];
    }

    static int getIfExistsBrutal(int[][] data, int row, int col) {
        try {
            return data[row][col];
        } catch (ArrayIndexOutOfBoundsException ex) {
            return 0; // does not exist
        }
    }

    static int sumOfCross(int[][] data, int row, int col) {
        // return sum of the element at row/col
        // plus (if they exist) element above, below, to the left and right of it
        int sum = getIfExistsGentle(data, row, col)
                + getIfExistsGentle(data, row - 1, col)
                + getIfExistsGentle(data, row + 1, col)
                + getIfExistsGentle(data, row, col - 1)
                + getIfExistsGentle(data, row, col + 1);
        return sum;
    }

    static void print2D(int[][] data) {
        int maxChars = 1;
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                int len = (data[row][col] + "").length();
                maxChars = len > maxChars ? len : maxChars;
            }
        }
        for (int row = 0; row < data.length; row++) {
            System.out.print("[");
            for (int col = 0; col < data[row].length; col++) {
                System.out.printf("%s%" + maxChars + "d", col == 0 ? "" : ", ", data[row][col]);
            }
            System.out.println(" ]");
        }
    }

    static int[][] duplicateArray2D(int[][] orig2d) {
        int[][] dup2d;
        dup2d = new int[orig2d.length][];
        for (int row = 0; row < orig2d.length; row++) {
            dup2d[row] = new int[orig2d[row].length];
        }
        return dup2d;
    }

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        int[][] data2D = { // JAGGED ARRAY
            {11, 3, 6, 8, 7, 1, 12},
            {17, 1, 8},
            {18, 3, -21, 1, 3, 12},
            {11, 7, 1, 19},};
        // PART A: Using sumOfCross() method write a search that will find
        // which element at row/col has the smallest sum of itself
        // and elements surrounding it.
        int smallestSum = sumOfCross(data2D, 0, 0);
        int smRow = 0, smCol = 0;
        // int smallestSum = Integer.MAX_VALUE;
        for (int row = 0; row < data2D.length; row++) {
            for (int col = 0; col < data2D[row].length; col++) {
                int sum = sumOfCross(data2D, row, col);
                if (sum < smallestSum) {
                    smallestSum = sum;
                    smRow = row;
                    smCol = col;
                }
            }
        }
        System.out.println("ORIGINAL ARRAY:");
        print2D(data2D);
        System.out.printf("Element at row=%d, col=%d has smallest cross-sum %d\n",
                smRow, smCol, smallestSum);
        // PART B: Create an integer array data2Dsums of identical size to data2D where
        // each element of it is the cross-sum of the element in the original array.
        // then print out the original, and print out the new array
        int[][] dup2d = duplicateArray2D(data2D);
        for (int row = 0; row < data2D.length; row++) {
            for (int col = 0; col < data2D[row].length; col++) {
                int sum = sumOfCross(data2D, row, col);
                dup2d[row][col] = sum;
            }
        }
        System.out.println("ARRAY OF CROSS-SUMS:");
        print2D(dup2d);
        // PART C: Ask user for integer value. In data2D array find all pairs of
        // values whose sum is the value entered by user and print them out.
        System.out.print("Enter sum you're looking for: ");
        int sumWanted = input.nextInt();
        
        
    }

}
