package day02wordsfreq;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Day02WordsFreq {

    static HashMap<String, Integer> wordFreqMap = new HashMap<>();

    public static void main(String[] args) {
        long startTimeTotal = System.currentTimeMillis();

        try ( Scanner fileInput = new Scanner(new File("genesis.txt"))) {
            fileInput.useDelimiter("[ \t\n\r\\.,:;-]");
            while (fileInput.hasNext()) {
                String word = fileInput.next();
                // System.out.print(word + "|");
                if (word.isEmpty()) {
                    continue;
                }
                wordFreqMap.putIfAbsent(word, 0);
                // TODO: maybe we can optimize this for speed somehow???
                int freq = wordFreqMap.get(word);
                wordFreqMap.put(word, freq + 1);
            }
        } catch (IOException ex) {
            System.out.println("File error: " + ex);
        }
        for (String word : new String[]{"God", "LORD", "called", "blessing"}) {
            System.out.printf("Word %s occurs %d\n", word, wordFreqMap.get(word));
        }

        long endTimeTotal = System.currentTimeMillis();
        double durationTotal = (endTimeTotal - startTimeTotal) / 1000.0;
        System.out.printf("Total runtime is %.3fs\n", durationTotal);
    }

}
